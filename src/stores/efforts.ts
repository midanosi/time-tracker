// @ts-nocheck
import { writable, derived, get } from 'svelte/store'
import { formatISO, addDays } from 'date-fns'
import { nanoid } from 'nanoid'

import { Effort } from '../types/types.type.ts'
import { effortsDB } from '../db'
import * as R from 'ramda'

import { selectedWeekDate as selectedWeekDateStore } from './dates'

function createWeekEffortsStore() {
  const store = writable<Effort[]>([])

  // on ANY change, update the entire store
  const refreshStoreVals = async () => {
    const monday = get(selectedWeekDateStore)
    const friday = addDays(monday, 4)
    let weekDocs = await effortsDB.find({
      selector: {
        dateISO: {
          $gte: formatISO(monday, { representation: 'date' }),
          $lte: formatISO(friday, { representation: 'date' }),
        },
      },
      sort: ['dateISO'],
    })
    weekDocs = weekDocs.docs
    const addDateField = R.map((doc) =>
      R.assoc('date', new Date(doc?.dateISO), doc)
    )
    weekDocs = addDateField(weekDocs) // don't really need this but whatever

    store.set(weekDocs)
  }
  // on change in selectedWeekDate, fetch different selection from pouchdb
  selectedWeekDateStore.subscribe(refreshStoreVals)

  store.add = async (effort: Effort): Promise<Effort[]> => {
    effort._id = `${effort.dateISO}-${effort.taskCode}-${nanoid()}`
    const add = await effortsDB.put(effort)
    if (add.ok) {
      refreshStoreVals()
    }
  }
  store.update = async (effortDoc): Promise<Effort[]> => {
    const update = await effortsDB.put(effortDoc)
    if (update.ok) {
      refreshStoreVals()
    }
  }
  store.remove = async (effortDoc): Promise<Effort[]> => {
    const remove = await effortsDB.remove(effortDoc)
    if (remove.ok) {
      refreshStoreVals()
    }
  }
  refreshStoreVals()

  return store
}
export const week_efforts = createWeekEffortsStore()

export const week_total_hours = derived(week_efforts, ($week_efforts) => {
  const sumDurations = R.compose(R.sum, R.map(R.prop('duration')))
  return sumDurations($week_efforts)
})

export const week_efforts_byDay = derived(week_efforts, ($week_efforts) => {
  const byDay = R.groupBy(R.prop('dateISO'))
  return byDay($week_efforts)
})
export const week_efforts_byTask = derived(week_efforts, ($week_efforts) => {
  const byTask = R.groupBy(R.prop('taskCode'))
  return byTask($week_efforts)
})
