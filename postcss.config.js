const tailwind = require('tailwindcss')
const autoprefixer = require('autoprefixer')
const cssnano = require('cssnano')
const atImport = require('postcss-import')

const plugins =
  process.env.NODE_ENV === 'production'
    ? [tailwind, atImport, cssnano]
    : [tailwind, atImport]

// const plugins =
//   process.env.NODE_ENV === 'production'
//     ? [tailwind(), autoprefixer(), atImport(), cssnano()]
//     : [tailwind(), autoprefixer(), atImport()]

// const plugins = [autoprefixer, atImport, cssnano, tailwind]
// const plugins = [require('autoprefixer')]

module.exports = { plugins }
