// @ts-nocheck
import { writable, derived } from 'svelte/store'
import { subDays, addDays } from 'date-fns'

import { today, prevMonday, allWeekDates } from '../utils/dateHelpers'

export const selectedDate = writable(today())

function selectedWeekStore(initialWeek) {
  const store = writable(initialWeek)

  function backOneWeek() {
    store.update((currentMonday) => {
      const prevMonday = subDays(currentMonday, 7)
      return prevMonday
    })
  }
  function forwardOneWeek() {
    store.update((currentMonday) => {
      const nextMonday = addDays(currentMonday, 7)
      return nextMonday
    })
  }

  return {
    backOneWeek,
    forwardOneWeek,
    subscribe: store.subscribe,
  }
}
export const selectedWeekDate = selectedWeekStore(prevMonday())

export const weekDates = derived<string[]>(
  selectedWeekDate,
  ($selectedWeekDate) => {
    return allWeekDates($selectedWeekDate)
  }
)
