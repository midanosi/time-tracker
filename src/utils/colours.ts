export const colours = {
  blue: 'rgba(50, 150, 255, 0.8)',
  bluepale: 'rgba(179, 225, 227, 0.8)',
  turqouise: 'rgba(123, 233, 194, 0.8)',
  greenlime: 'rgba(127, 254, 166, 0.8)',
  greendark: 'rgba(100, 205, 100, 0.8)',
  green: 'rgba(202, 253, 108, 0.8)',
  yellow: 'rgba(253, 251, 105, 0.8)',
  yellowpale: 'rgba(241, 252, 201, 0.8)',
  brownlight: 'rgba(211, 202, 181, 0.8)',
  pink: 'rgba(230, 104, 194, 0.8)',
  brown: 'rgba(178, 109, 127, 0.8)',
  orange: 'rgba(248, 116, 145, 0.8)',
  red: 'rgba(225, 80, 100, 0.8)',
  purple: 'rgba(140, 135, 243, 0.8)',
  violet: 'rgba(185, 162, 243, 0.8)',
} as const

export const coloursRGBA: string[] = Object.values(colours)
