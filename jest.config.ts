import jestConfig from "@snowpack/app-scripts-svelte/jest.config.js"

module.exports = {
  ...jestConfig(),
};
