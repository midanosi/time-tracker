module.exports = {
  purge: ['./src/**/*.svelte', './public/*.html'],
  theme: {
    extend: {
      inset: {
        100: '100%',
        '1/4': '25%',
        '1/2': '50%',
        '3/4': '76%',
      },
    },
    opacity: {
      0: '0',
      25: '.25',
      50: '.5',
      75: '.75',
      10: '.1',
      20: '.2',
      30: '.3',
      40: '.4',
      60: '.6',
      70: '.7',
      80: '.8',
      85: '.85',
      90: '.9',
      100: '1',
    },
  },
  variants: {
    extend: {
      opacity: ['disabled'],
    },
  },
  plugins: [require('tailwind-percentage-heights-plugin')()],
}
