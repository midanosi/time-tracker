import PouchDB from 'pouchdb-browser'
import PouchDBFind from 'pouchdb-find'
import type { Task } from './types/types.type'
import { colours } from './utils/colours'

export const effortsDB = new PouchDB('efforts')
export const tasksDB = new PouchDB('tasks')

const defaultTasks: Task[] = [
  {
    _id: '00004',
    code: '00004',
    name: 'Holiday',
    colour: colours.greenlime,
  },
  {
    _id: '00003',
    code: '00003',
    name: 'Sick Leave',
    colour: colours.yellow,
  },
  {
    _id: '000046',
    code: '000046',
    name: 'Covid (unproductive)',
    colour: colours.yellow,
  },
  {
    _id: '7000-00',
    code: '7000-00',
    name: 'Training Received',
    colour: colours.pink,
  },
  {
    _id: '7000-03',
    code: '7000-03',
    name: 'Self learning',
    colour: colours.pink,
  },
  {
    _id: '8100',
    code: '8100',
    name: 'Meetings & Togetherness',
    colour: colours.blue,
  },
  {
    _id: '8500',
    code: '8500',
    name: 'Reviews',
    colour: colours.blue,
  },
]
tasksDB.bulkDocs(defaultTasks)

const myProjects = [
  {
    _id: 'p3497',
    code: 'p3497',
    name: 'Wind Waker',
    shorthand: 'ww',
    deltekSubcode: '02',
    deltekTaskCode: '02',
    isProject: true,
    colour: colours.greendark,
  },
  {
    _id: 'p4280',
    code: 'p4280',
    name: 'Arbuckle Phase 3',
    shorthand: 'arb',
    deltekSubcode: '05',
    deltekTaskCode: '02',
    isProject: true,
    colour: colours.purple,
  },
  {
    _id: 'p3650',
    code: 'p3650',
    name: 'Tobago',
    shorthand: 'tob',
    deltekSubcode: '01', // is guess
    deltekTaskCode: '01', // is guess
    isProject: true,
    // colour: colours.bluepale,
  },
]
tasksDB.bulkDocs(myProjects)

PouchDB.plugin(PouchDBFind)
effortsDB.createIndex({
  index: {
    fields: ['dateISO'],
  },
})
