// @ts-nocheck
import { readable } from 'svelte/store'

export const timeSettings = readable({
  hourWidth: 48, // px
  earliestHour: 8,
  latestHour: 21,
})
