export type Task = {
  name: string
  code: string
  shorthand?: string
  deltekSubcode?: string
  deltekTaskCode?: string
  isProject?: boolean
  colour: string
  _id: string
  _rev?: string
}

export type Effort = {
  taskCode?: string
  start: number // 0 - 24, where 0.25, 0.5, 0.75 possible
  duration: number // 15, 30, 45 etc, in hours, so 0.25, 0.5, 0.75, 1 only
  dateISO: string
  date?: Date
  description?: string
  _id: string
  _rev?: string
}
