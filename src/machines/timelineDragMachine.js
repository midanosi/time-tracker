import { Machine } from 'xstate'

const timelineDragMachine = Machine({
  id: 'timelineDrag',
  initial: 'idle',
  states: {
    idle: {
      on: {
        MOUSEOVER: 'hovered',
      },
    },
  },
})
