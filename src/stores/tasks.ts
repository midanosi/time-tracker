import { writable, derived } from 'svelte/store'
import * as R from 'ramda'

import { tasksDB } from '../db'
import type { Task } from '../types/types.type'

// background-grey: hsl(221, 14%, 36%)

function createTasksStore() {
  const store = writable<Task[]>([])

  const refreshStoreVals = async () => {
    let taskDocs = await tasksDB.allDocs({
      include_docs: true,
    })
    taskDocs = taskDocs.rows
    taskDocs = R.map(R.prop('doc'))(taskDocs)
    store.set(taskDocs)
  }

  refreshStoreVals()

  store.add = async (task: Task): Promise<Task[]> => {
    task._id = task.code
    const add = await tasksDB.put(task)
    if (add.ok) {
      refreshStoreVals()
    }
  }
  store.update = async (taskDoc): Promise<Task[]> => {
    const update = await tasksDB.put(taskDoc)
    if (update.ok) {
      refreshStoreVals()
    }
  }
  store.remove = async (taskDoc): Promise<Task[]> => {
    const remove = await tasksDB.remove(taskDoc)
    if (remove.ok) {
      refreshStoreVals()
    }
  }

  return store
}

export const tasks = createTasksStore()

export const project_tasks = derived(tasks, ($tasks) => {
  const onlyProjects = R.filter(R.propEq('isProject', true))
  return onlyProjects($tasks)
})
export const nonProject_tasks = derived(tasks, ($tasks) => {
  const notProjects = R.filter(R.propSatisfies((x) => !x, 'isProject'))
  return notProjects($tasks)
})

export const tasks_byCode = derived(tasks, ($tasks) => {
  const indexByCode = R.indexBy(R.prop('code'))
  return indexByCode($tasks)
})
