// eslint-disable-next-line @typescript-eslint/no-var-requires
const sveltePreprocess = require('svelte-preprocess')

module.exports = {
  preprocess: sveltePreprocess({
    defaults: {
      script: 'typescript',
    },
    postcss: true,
    // postcss: {
    //   plugins: [require('tailwindcss')()],
    // },
  }),
}
