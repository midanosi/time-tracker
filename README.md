# Time tracker (for projects) 

Functionality: track time spent on projects easily

Purpose of creation: to make an app using bunch of latest technologies/libraries that I/we lack experience in; Svelte, Typescript, pnpm, Snowpack, Tailwind

## Scripts

### `pnpm start` / `snowpack dev`

Runs the app in the development mode.
Open http://localhost:8080 to view it in the browser.

The page will reload if you make edits.

### `pnpm test` / `jest`

Launches the test runner in the interactive watch mode.

### `pnpm run build` / `snowpack build`

Builds a static copy of your site to the `build/` folder.
