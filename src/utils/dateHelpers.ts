import { startOfToday, addDays, format } from 'date-fns'
import * as R from 'ramda'

export const today = (): Date => {
  const today = startOfToday()
  return today
}

export const prevMonday = (): Date => {
  const today = startOfToday()
  const dayNum = today.getDay()

  let monday: Date
  if (dayNum === 0) {
    monday = today
  } else {
    monday = new Date()
    monday.setDate(today.getDate() - (dayNum - 1))
  }
  return monday
}

export const allWeekDates = (mondayDate: Date): Date[] => {
  const weekDates = [
    mondayDate,
    addDays(mondayDate, 1),
    addDays(mondayDate, 2),
    addDays(mondayDate, 3),
    addDays(mondayDate, 4),
    // addDays(mondayDate, 5),
    // addDays(mondayDate, 6),
  ]
  return R.map((date: Date) => format(date, 'yyyy-MM-dd'), weekDates)
}
