declare module 'ramda'
declare module 'lodash.range'

declare module 'pouchdb-browser'
declare module 'pouchdb-find'
declare module 'nanoid'

declare module 'chroma-js'

declare module 'sveltejs-tippy'

declare module 'chart.xkcd'
declare module 'xstate-svelte'
